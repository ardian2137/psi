
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Scanner;

public class OX {
	
	public static void networkLearn(Network network) {
		File loadPath = new File ("files/dane.txt");
		int combCount = 58;
		double[][] Combinations = new double[combCount][9];
		double[][] results = new double[combCount][1];
		try {	
			Scanner loadScanner = new Scanner(loadPath);
			while(loadScanner.hasNext()) {
				
				for(int i=0;i<combCount;i++) {
					for(int j=0;j<9;j++) {
						Combinations[i][j]=loadScanner.nextDouble();
					}
					results[i][0]=loadScanner.nextDouble();
				}			
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("Cos, cos sie popsulo");
		}
		System.out.println(combCount);
							
						        network.RandomWeights(-0.65,0.75);
						        double n=0.09;
						        int i=0;
						        boolean isDone=false;

						        double acceptableError=0.01;
						       
						        while(!isDone && i<50000){
						            i++;
						            isDone=true;

						            for(int j=0;j<combCount;j++) {
						            network.Teach(results[j], Combinations[j], n);			
						            }
						           for(int k=0;k<combCount;k++) {
							            if((Math.abs(results[k][0]-network.CountOutput(Combinations[k])[0])>acceptableError)){
							                isDone=false;
							      
						            }
						           }
						      
						        }	
//						        int finalResults[] = new int[combCount];
//						        for(int j=0;j<combCount;j++) {
//						        finalResults[j]=(int) Math.round(9*network.CountOutput(Combinations[j])[0]);
//						            System.out.println("results["+j+"]="+finalResults[j]);
//						            }
	};
	
	public static int networkMove(char charTab[],int tab[],Network network,char c) {
		double[][] Combination=new double[1][9];
		for(int i=0;i<9;i++) {
			Combination[0][i]=tab[i];
		}
		double q=network.CountOutput(Combination[0])[0];
		System.out.println(q);
		int p = (int) Math.round(9*network.CountOutput(Combination[0])[0]);
		System.out.println(p);
		return p;
	}
	public static  boolean win(char charTab[],char sign,int size){
		for(int i=0;i<3;i++) {
			if(charTab[i]==sign && charTab[i+3]==sign && charTab[i+6]==sign) {
				System.out.println("Wygra�:"+sign);
				return true;
			}
			if(charTab[3*i]==sign && charTab[3*i+1]==sign && charTab[3*i+2]==sign) {
				System.out.println("Wygra�:"+sign);
				return true;
			}
			else if(i==0) {
				if(charTab[i]==sign && charTab[i+4]==sign && charTab[i+8]==sign) {
					System.out.println("Wygra�:"+sign);
				return true;
				}
			}	
			else if(i==2) {
				if(charTab[i]==sign && charTab[i+2]==sign && charTab[i+4]==sign) {
					System.out.println("Wygra�:"+sign);
				return true;
				}
			}	
		}
		int takenPlaces=0;
		for(int i=0;i<size;i++) {
			if(charTab[i]!=' ')
				takenPlaces++;		
		}
		if(takenPlaces==9){
			System.out.println("Remis");
		return true;
		}
		else return false;
		}
	
	public static void drawBoard(int size,int tab[],char charTab[]) {
		for(int i=0;i<9;i+=3) {
			System.out.println(charTab[i]+" | "+charTab[i+1]+" | "+charTab[i+2]);
			if(i!=6)
				System.out.println( "__  "+"__  "+"__ ");
		}
	}
	
	public static boolean move(int p,int tab[],char charTab[],char c) {
		if(tab[p]==0) {
			if(c=='X')
			tab[p]=1;
			else
				tab[p]=2;
			
			charTab[p]=c;
			return true;
			}
		else
			return false;
	}
	
	public static void clearBoard(int size,char charTab[],int tab[] ) {
		for(int i=0;i<size;i++) {
			tab[i]=0;
			charTab[i]=' ';
		}		
	}
	
		public static void main(String args[]) throws IOException {
			Random generator = new Random();
			Network network = new Network(9,10,1,2);
			networkLearn(network);
			boolean error=false;
			int playerChar=-1,computerChar=1,playerMove=-100,networkMove=-100;//-1-k�ko 1-krzy�yk
			char X='X',O='O';
			boolean win=false,OX=false;
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			int size=9;
			int tab[]=new int[size];
			char charTab[]=new char[9]; 
			clearBoard( size,charTab,tab);			      				   
			
			drawBoard(size,tab,charTab);
			OX=false;
			while(!win) {
					
					while(!OX) {
					if(error==false) {
			        networkMove = networkMove(charTab,tab,network,X);
					}
					else {
						networkMove=generator.nextInt(9);
					}
					//System.out.println(networkMove);
				if(networkMove>-1 && networkMove <9 && tab[networkMove]==0 || networkMove <-1 || networkMove >9) {
					OX=true;
				}
				else {
					error=true;
					//System.err.println("Bledny ruch!");
				}
			}
			OX=false;
			error=false;
			move(networkMove,tab,charTab,X);	
			drawBoard(size,tab,charTab);
			win=win(charTab,X,size);
			if(!win) {
			while(!OX) {
				 try{
		             playerMove = Integer.parseInt(br.readLine());
		        }catch(NumberFormatException e){
		            System.err.println("Ale liczbe calkowita to ty podaj!");
		        }
				System.out.println(playerMove);
			if(playerMove>-1 && playerMove <9 && tab[playerMove]==0 || playerMove <-1 || playerMove >9) {
				OX=true;
			}
			else {
				System.err.println("Bledny ruch!");
			}
		}
		OX=false;
		move(playerMove,tab,charTab,O);	
		drawBoard(size,tab,charTab);
		win=win(charTab,O,size);
				}
			}
			System.exit(0); 
	}
}
